angular.module("starter")
.controller('HomeCtrl', HomeCtrl)

HomeCtrl.$inject = ["$ionicPlatform", "$cordovaImagePicker", "$cordovaCamera", "$cordovaFacebook", "$cordovaSocialSharing"];

function HomeCtrl($ionicPlatform, $cordovaImagePicker, $cordovaCamera, $cordovaFacebook, $cordovaSocialSharing){
	var vm = this;

	vm.defaultImage = "http://www.cultivatesmb.com/wp-content/plugins/post-grid/assets/frontend/css/images/placeholder.png";
	vm.selectImage = function(){
		var options = {
		   maximumImagesCount: 1,
		   width: 800,
		   height: 800,
		   quality: 80
		};

		$cordovaImagePicker.getPictures(options)
		.then(function (results) {
			for (var i = 0; i < results.length; i++) {
				console.log('Image URI: ' + results[i]);
				vm.defaultImage = results[i];
			}
		}, function(error) {
			// error getting photos
		});
	}

	vm.takePicture = function(){
		var options = {
			destinationType: Camera.DestinationType.FILE_URI,
			sourceType: Camera.PictureSourceType.CAMERA,
		};  

		$cordovaCamera.getPicture(options).then(function(imageURI) {
			vm.defaultImage = imageURI;
		}, function(err) {
			// error do something
		});
	}

	//Login FB:
	/*
	{"status":"connected","authResponse":{"accessToken":"EAAEB4O7fZCHoBAHxbZC0dcQFCUSDOcTQIRAkaqM5bxjCE1EcVOuu8UJTMhBQMpBExxKLytBTLp0vra6p6sHes66Clz4wG0beqawTZCStEGNCR6cEp32fYwZCTHtd6kBZClfl16MRCj2qI1NMOBygDsa9swi4MtnuCspK4ZBuO5KQZDZD","expiresIn":"5183377","session_key":true,"sig":"...","userID":"211293779315682"}}
	*/

	vm.fullname = "";
	vm.userid = "";
	vm.loginFB = function(){
		$cordovaFacebook.login(["public_profile", "email", "user_friends"])
	    .then(function(success) {

	    	$cordovaFacebook.api("me", ["public_profile"])
		    .then(function(success) {
		    	//{"name":"Fsf Singapore","id":"211293779315682"}"
		    	vm.fullname = success.name;
	    		vm.userid = success.id;
		    }, function (error) {
		      // error
		    });
		    
	    }, function (error) {
	      // error
	    });
	}

	vm.shareThis = function(){
		$cordovaSocialSharing
		.share(null, null, null, "http://google.com")
		.then(function(result){
			
		}, function(err){
			//do something
		});
	}
}