/**
 * Created by phangty on 10/11/16.
 */

(function () {

angular.module('weddingGramApp').factory('AuthFactory',
    ['$q', '$timeout', '$http', 'Flash', '$state',
        function ($q, $timeout, $http, Flash, $state) {

            // create user variable
            var user = null;
            // return available functions for use in the controllers
            return ({
                isLoggedIn: isLoggedIn,
                getUserStatus: getUserStatus,
                login: login,
                logout: logout,
                register: register
            });

            function isLoggedIn() {
                if(user) {
                    return true;
                } else {
                    return false;
                }
            }

            function getUserStatus(callback) {
                $http.get("http://weddinggram.getsandbox.com/user/status")
                // handle success
                    .then(function (data) {
                        console.log(data);
                        var authResult = JSON.stringify(data);
                        if(data["data"] != ''){
                            user = true;
                            callback(user);
                        } else {
                            user = false;
                            callback(user);
                        }
                    });
            }

            function login(userProfile) {

            }

            function logout() {

            }

            function register(username, password) {

            }
        }]);
})();