angular.module("starter")
.controller('HomeCtrl', HomeCtrl)

HomeCtrl.$inject = ["$ionicPlatform", "$ionicLoading", "$state", "$scope", "$timeout", "$ionicPopup", "$ionicActionSheet", "SignUpModal"];

function HomeCtrl($ionicPlatform, $ionicLoading, $state, $scope, $timeout, $ionicPopup, $ionicActionSheet, SignUpModal){
	var vm = this;

	vm.submitBtn = function(){
		$ionicLoading.show({
          template: '<ion-spinner icon="android"></ion-spinner>'
        });

        $timeout(function() {
	    	$ionicLoading.hide();
	   	}, 3000); //after 3s
	};

	//list:
	vm.users = [
		"User A",
		"User B"
	];

	//update list:
	vm.updateList = function(){
		$timeout(function() {
			//manipulate the records:
			vm.users = [
				"User A",
				"User B",
				"User C"
			];

			// Stop the ion-refresher from spinning
			$scope.$broadcast('scroll.refreshComplete');
		}, 3000); //after 3s
	};

	vm.shareBtn = function(){
		$ionicActionSheet.show({
			buttons: [
				{ text: 'Facebook' },
				{ text: 'Twitter' }
			],
			titleText: 'Shares',
			cancelText: 'Cancel',
			cancel: function() {
				// add cancel code..
			},
			buttonClicked: function(index) {
				//check index for certain action:
				//if (index == 0) {}
				console.log(index);
				return true;
			},
			destructiveButtonClicked: function(){
				console.log("destructive");
				return true;
			}
		});
	}

	vm.showSignUp = function(){
		SignUpModal
			.init($scope)
			.then(function(mdl){
				mdl.show();
			});
	}

	vm.confirmationBtn = function(){
		var confirmPopup = $ionicPopup.confirm({
			title: 'Watch out!',
			template: 'Exit this app?'
		});

		confirmPopup.then(function(res) {
			if(res) {
				console.log('Ok');
			} else {
				console.log('Cancel');
			}
		});
	}

	//infinite scroll:
	vm.lists = [
		"User 1",
		"User 2",
		"User 3",
		"User 4"
	];
	vm.stillHaveData = true;
	vm.loadMoreDatas = function(){
		var nextUser = vm.lists.length + 1;
		vm.lists.push("User " + nextUser);
		if(vm.lists.length > 30){
			vm.stillHaveData = false;
		}

		//stop infinite scroll after being executed:
		$scope.$broadcast('scroll.infiniteScrollComplete');
	}
}