angular
.module("starter")
.factory('SignUpModal',['$ionicModal', '$rootScope',
    function ($ionicModal, $rootScope) {

    	function initialize($scope){
    		var vmparent = $scope;
    		var vm = $scope || $rootScope.$new();

    		// create modal variable
	        var promise = $ionicModal
	        .fromTemplateUrl('templates/signupmodal.html', {
		      scope: $scope,
		      animation: 'slide-in-up'
		    }).then(function(modal){
		      $scope.modal = modal;
		      return modal;
		    });

		    $scope.closeMe = function(){
		    	$scope.modal.hide();
		    }

		    return promise;
    	}

	    return ({
            init: initialize
        });
    }]);