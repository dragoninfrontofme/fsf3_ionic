(function () {
    angular
        .module("weddingGramApp")
        .controller("CommentListCtrl", ["$http",'$state' ,CommentListCtrl]);

    function CommentListCtrl($http,$state) {
        var self = this;

        self.commentForm = {};
        self.comments = [];

        self.getComments = function (postId) {
            $http
                .get("http://weddinggram.getsandbox.com/api/posts/comment")
                .then(function (respose) {
                    self.comments = respose.data;
                })
                .catch(function () {
                })

        };

        self.showComments= function (post) {

        };
        
        self.addComment = function (postId) {
            console.log(self.commentForm);
        };
    }
})();
