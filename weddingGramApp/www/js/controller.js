(function () {
    angular
        .module("weddingGramApp")
        .controller("SettingCtrl", ["$scope", SettingCtrl]);

    function SettingCtrl($scope) {
        var self = this;

        self.myLists = [];

        self.updateList = function(){
            console.log("pulled");
            self.myLists = [
                1,2,3,4,5,6,7,8,9,10
            ];
            
            $scope.$broadcast('scroll.refreshComplete');
        };

    }
})();