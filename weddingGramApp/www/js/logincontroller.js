(function () {
    angular
        .module("weddingGramApp")
        .controller("LoginCtrl", ["$scope", "$cordovaFacebook", LoginCtrl]);

    function LoginCtrl($scope, $cordovaFacebook) {
        var self = this;

        self.loginFB = function(){

            $cordovaFacebook
                .login(["public_profile", "email", "user_friends"])
                .then(function(success){

                    $cordovaFacebook
                        .api("me", ["public_profile"])
                        .then(function(success){

                            //{"name":"Fsf Singapore","id":"211293779315682"}"
                            alert(success.name);
                        }, function(err){
                            //do somethings
                        }); 

                }, function(error){
                    //do something
                });

        };

    }
})();