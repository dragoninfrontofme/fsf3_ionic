(function () {
    angular
        .module("weddingGramApp")
        .controller("PostListCtrl", ["PostAPI", "$http", "$ionicActionSheet", "$ionicLoading", "$timeout", "SignUpModal", "$scope", "$cordovaImagePicker", "$cordovaCamera", "$cordovaSocialSharing", PostListCtrl]);

    function PostListCtrl(PostAPI, $http, $ionicActionSheet, $ionicLoading, $timeout, SignUpModal, $scope, $cordovaImagePicker, $cordovaCamera, $cordovaSocialSharing) {
        var self = this;

        self.myImage = "";
        self.getImage = function(){
            var options = {
               maximumImagesCount: 1,
               width: 800,
               height: 800,
               quality: 80
            };

            $cordovaImagePicker
                .getPictures(options)
                .then(function(results){
                    //results = array()
                    self.myImage = results[0];

                }, function(err){
                    //do something
                });
        };

        self.takeImage = function(){
            var options = {
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA,
            };

            $cordovaCamera
                .getPicture(options)
                .then(function(imageuri){
                    self.myImage = imageuri;
                }, function(err){
                    //do something
                });
        };

        self.shareButton = function(){
            //share(message, subject, file, link)

            $cordovaSocialSharing
                .share(null, null, null, "http://google.com")
                .then(function(result){
                    //do something
                }, function(err){
                    //do something
                });
        };

        $ionicLoading.show({
          template: '<ion-spinner icon="android"></ion-spinner>'
        });

        PostAPI
            .me()
            .then(function (response) {
                $ionicLoading.hide();
                self.posts = response.data;
            })
            .catch(function (err) {
                console.log(err);
            });

        self.like = function (post) {
            
        };

        self.likeButton = function(){
            SignUpModal
                .init($scope)
                .then(function(md){
                    md.show();
                });

            /*$ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
            });

            $timeout(function() {
                $ionicLoading.hide();
            }, 3000); //after 3s*/
        }


        self.showActionSheet = function(){
            console.log("Clicked");

            $ionicActionSheet.show({
                    buttons: [
                        { text: 'Facebook' },
                        { text: 'Twitter' },
                        { text: 'A' },
                        { text: 'B' }
                    ],
                    titleText: 'Shares',
                    //cancelText: 'Cancel',
                    destructiveText: 'Remove',
                    cancel: function() {
                        // add cancel code..

                    },
                    buttonClicked: function(index) {
                        //check index for certain action:
                        //if (index == 0) {}
                        console.log(index);

                        if(index == 3){
                            //
                        }

                        return true;
                    },
                    destructiveButtonClicked: function(){
                        console.log("destructive");

                        return true;
                    }
            });
        }

    }
})();