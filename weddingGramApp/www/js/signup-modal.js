(function () {
    angular
    .module("weddingGramApp")
    .factory('SignUpModal', ["$ionicModal", "$rootScope",
        function($ionicModal, $rootScope){

            function initialize($scope){
                var vmparent = $scope;
                var vm = $scope || $rootScope.$new();

                var promise = $ionicModal
                .fromTemplateUrl('templates/signupmodal.html', {
                    scope: vm,
                    animation: 'slide-in-up'
                }) 
                .then(function(modalvar){
                    vm.modal = modalvar;
                    return modalvar;
                });

                vm.closeMe = function(){
                    vm.modal.hide();
                };

                return promise;
            }

            return ({
                init: initialize
            });
        }
    ]);

})();