// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('weddingGramApp', ['ionic', 'ngCordova'])
.run(function($ionicPlatform, $cordovaPush, $rootScope) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });

  //notification settings:
  document.addEventListener("deviceready", function(){ 
    var androidConfig = {"senderID": "50876164056", 'alert': true};
    $cordovaPush.register(androidConfig).then(function(result) {
      // Success
      console.log("Device registered!" + result);
    }, function(err) {
      // Do Something   
    });

    $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
      switch(notification.event) {
        case 'registered':
          if (notification.regid.length > 0 ) {
            console.log("My Device registered! " + notification.regid);
          }
          break;
        case 'message':
          // this is the actual push notification. its format depends on the data model from the push server
          //route user to particular
          break;
        case 'error':
          console.log('GCM error = ' + notification.msg);
          break;
        default:
          console.log('An unknown GCM event has occurred');
          break;
      }
    });
  });

})
.config(function($stateProvider, $urlRouterProvider){
  $stateProvider
    .state('side', {
      url: '/side',
      abstract: true,
      templateUrl: "templates/sides.html"
    })
    .state('side.wedding', {
      url: '/wedding',
      views: {
        'mainContainer': {
          templateUrl: "templates/wedding-gram.html"
        }
      } 
    })
    .state('side.settings', {
      url: '/settings',
      views: {
        'mainContainer': {
          templateUrl: "templates/settings.html"
        }
      } 
    })
    .state('side.login', {
      url: '/login',
      views: {
        'mainContainer': {
          templateUrl: "templates/login.html"
        }
      } 
    });

    /*.state('tab', {
      url: '/tab',
      abstract: true,
      templateUrl: "templates/tabs.html"
    })
    .state('tab.wedding', {
      url: '/wedding',
      views: {
        'tab-wedding': {
          templateUrl: "templates/wedding-gram.html"
        }
      } 
    })
    .state('tab.settings', {
      url: '/settings',
      views: {
        'tab-settings': {
          templateUrl: "templates/settings.html"
        }
      } 
    });*/

    /*.state("wedding", {
      url: "/wedding",
      templateUrl: "templates/wedding-gram.html",
    });*/

    $urlRouterProvider.otherwise('/side/wedding');
});


