(function () {
    angular
        .module("example2")
        .controller("LoginCtrl", LoginCtrl)
        .controller("HomeCtrl", HomeCtrl);

    LoginCtrl.$inject = ["$ionicPlatform", "$http", "$state"];

    function LoginCtrl($ionicPlatform, $http, $state){
        var vm = this;

        vm.user = {
            email: '',
            password: ''
        }

        vm.doLogin = function(){
            $http({
              method: 'POST',
              url: "http://demo5964607.mockable.io/login",
              data: {"email":vm.user.email, "password":vm.user.password},
              headers: {
               'Content-Type': "application/json"
              },
            }).then(function successCallback(response){
              if(response.status == 200){
                $state.go("home", {'fullName':'FSF'});
              }
            }, function errorCallback(response) {
              console.log("Err: " + response)
            });
        }


        $ionicPlatform.ready(function(){
            console.log("Entered!");
        });

    }

    HomeCtrl.$inject = ["$ionicPlatform", "$state", "$stateParams"];

    function HomeCtrl($ionicPlatform, $state, $stateParams){
        var vm = this;

        vm.message = "Welcome, " + $stateParams.fullName;

        $ionicPlatform.ready(function(){
            
        });

        vm.doLogout = function(){
            $state.go("login");
        }

    } 
})();