// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngCordova'])

.run(function($ionicPlatform, $cordovaPush, $rootScope) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });

  document.addEventListener("deviceready", function(){
    var androidConfig = {"senderID": "321598840309", 'alert': true};
    $cordovaPush.register(androidConfig).then(function(result) {
      // Success
      console.log("Device registered!" + result);

    }, function(err) {
      // Error
    });

    $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
      switch(notification.event) {
        case 'registered':
          if (notification.regid.length > 0 ) {
            console.log("My Device registered! " + notification.regid);
          }
          break;
        case 'message':
          // this is the actual push notification. its format depends on the data model from the push server
          break;
        case 'error':
          console.log('GCM error = ' + notification.msg);
          break;
        default:
          console.log('An unknown GCM event has occurred');
          break;
      }
    });
  });

  //APA91bGfeanCDhATWtpWOnJvUjt63VWoIq34BizKNUDPxxlzidvWGeDRC5gMFJAUmYlUUimvrS3iotn_EgHcCkCoga1rJ9dBq1WDEvHc0jvrT07GAsDwdX5M-gnzu5t0Pp4VGd93dRWq9lfhrz0c0Gms46jkxW-CVw

})

.config(function($stateProvider, $urlRouterProvider){
  $stateProvider
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })
  .state('tab.home', {
    url: '/home',
    views: {
      'tab-home': {
        templateUrl: 'templates/home.html'
      }
    }
  })
  .state('tab.settings', {
    url: '/settings',
    views: {
      'tab-settings': {
        templateUrl: 'templates/settings.html'
      }
    }
  });

  $urlRouterProvider.otherwise('/tab/home');
});
